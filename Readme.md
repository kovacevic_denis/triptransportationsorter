### Used Technologies
 - PHP7.1
 
### Used Libraries:
- https://getcomposer.org/ for autoloading,

### Installation, console run:
- git clone https://bitbucket.org/kovacevic_denis/triptransportationsorter
- cd triptransportationsorter
- composer dump-autoload
- php journey.php