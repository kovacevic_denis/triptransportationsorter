<?php

if (!function_exists('loadJson')) {
    function loadTrips()
    {
        $trips = [];
        $directory = __DIR__ . '/config/trips';
        $files = array_diff(scandir($directory), array('..', '.'));
        foreach ($files as $file) {
            $json = file_get_contents($directory . '/' . $file);
            $data = json_decode($json, true);
            $trips[$file] = $data;
        }

        return $trips;
    }
}