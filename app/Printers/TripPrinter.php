<?php

namespace App\Printers;

use App\Contracts\Printable;
use App\Transportation\AbstractTransportation;

class TripPrinter extends AbstractPrinter implements Printable
{
    /** @var TransportationPrinter[] */
    protected $transportationPrinters;

    /**
     * TripPrinter constructor.
     * @param AbstractTransportation[] $transportations
     */
    public function __construct(array $transportations)
    {
        foreach ($transportations as $transportation) {
            $this->transportationPrinters[] = new TransportationPrinter($transportation);
        }
    }

    /**
     * @return string
     */
    public function print(): string
    {
        $i = 1;
        $message = '';
        foreach ($this->transportationPrinters as $transportationPrinter) {
            $message .= $i . '. ' . $transportationPrinter->print() . "\n";
            $i++;
        }

        if(!empty($message)) {
            $message .= self::ARRIVED_MESSAGE . "\n";
        }

        return $message;
    }
}