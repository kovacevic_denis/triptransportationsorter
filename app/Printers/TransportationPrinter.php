<?php

namespace App\Printers;

use App\Contracts\Baggageable;
use App\Contracts\Gateable;
use App\Contracts\Printable;
use App\Transportation\AbstractTransportation;

/**
 * Class TransportationPrinter
 * @package App\Transportation
 */
class TransportationPrinter extends AbstractPrinter implements Printable
{
    /** @var AbstractTransportation */
    protected $transportation;

    /**
     * TransportationPrinter constructor.
     * @param AbstractTransportation $abstractTransportation
     */
    public function __construct(AbstractTransportation $abstractTransportation)
    {
        $this->transportation = $abstractTransportation;
    }

    /**
     * @return string
     */
    public function print(): string
    {
        $destinationMessage = $this->getDestinationMessage();
        $gateMessage = $this->getGateMessage();
        $seatMessage = $this->getSeatMessage();
        $baggageMessage = $this->getBaggageMessage();

        $message = $destinationMessage . $gateMessage . $seatMessage;
        if ($baggageMessage) {
            $message .= "\n" . $baggageMessage;
        }

        return $message;
    }

    /**
     * @return string
     */
    protected function getDestinationMessage(): string
    {
        $transportationType = $this->transportation->getType();
        $transportationNumber = $this->transportation->getNumber() ?? '';

        if($transportationNumber) {
            $transportationType .= ' ';
        }

        $from = $this->transportation->getFrom();
        $to = $this->transportation->getTo();

        return sprintf(self::DESTINATION_MESSAGE,
            $transportationType,
            $transportationNumber,
            $from,
            $to
        );
    }

    /**
     * @return string
     */
    protected function getSeatMessage(): string
    {
        $seatMessage = self::NO_SEAT_MESSAGE;
        if ($this->transportation->getSeat()) {
            $seatMessage = sprintf(self::SEAT_MESSAGE, $this->transportation->getSeat());
        }

        return $seatMessage;
    }

    /**
     * @return string
     */
    protected function getGateMessage(): string
    {
        $gateMessage = '';
        if ($this->transportation instanceof Gateable) {
            $gateMessage = sprintf(self::GATE_MESSAGE, $this->transportation->getGate());
        }

        return $gateMessage;
    }

    /**
     * @return string
     */
    protected function getBaggageMessage(): string
    {
        $baggageMessage = '';
        if ($this->transportation instanceof Baggageable) {
            $baggageMessage = self::AUTOMATIC_BAGGAGE_TRANSFER_MESSAGE;

            if ($this->transportation->getBaggageNumber()) {
                $baggageMessage = sprintf(self::BAGGAGE_TRANSFER_MESSAGE, $this->transportation->getBaggageNumber());
            }
        }

        return $baggageMessage;
    }
}