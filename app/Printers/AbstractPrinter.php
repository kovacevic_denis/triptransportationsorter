<?php

namespace App\Printers;

abstract class AbstractPrinter
{
    const DESTINATION_MESSAGE = 'Take %s%s from %s to %s.';
    const SEAT_MESSAGE = 'Sit in seat %s.';
    const GATE_MESSAGE = 'Gate %s.';
    const NO_SEAT_MESSAGE = 'No seat assignment';
    const BAGGAGE_TRANSFER_MESSAGE = 'Baggage drop at ticket counter %s.';
    const AUTOMATIC_BAGGAGE_TRANSFER_MESSAGE = 'Baggage will we automatically transferred from your last leg.';
    const ARRIVED_MESSAGE = 'You have arrived at your final destination.';
}