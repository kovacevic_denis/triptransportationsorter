<?php
namespace App\Contracts;

/**
 * Interface Gateable
 * @package App\Contracts
 */
interface Gateable
{
    public function getGate();
}