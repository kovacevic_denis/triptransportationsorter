<?php
namespace App\Contracts;

/**
 * Interface Baggageable
 * @package App\Contracts
 */
interface Baggageable
{
    public function getBaggageNumber();
}