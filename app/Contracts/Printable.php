<?php

namespace App\Contracts;

/**
 * Interface Printable
 * @package App\Contracts
 */
interface Printable
{
    public function print(): string;
}