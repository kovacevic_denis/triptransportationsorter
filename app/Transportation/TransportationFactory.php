<?php

namespace App\Transportation;

use App\Exceptions\TransportationNotExistException;

/**
 * Class TransportationFactory
 * @package App\Transportation
 */
class TransportationFactory
{

    public static $destinationsWeight;

    /**
     * @param string $transportation
     * @return AbstractTransportation
     * @throws TransportationNotExistException
     */
    public static function make(string $transportation): AbstractTransportation
    {
        $transportation = __NAMESPACE__ . '\\' . ucfirst($transportation) . 'Transportation';
        if (class_exists($transportation)) {
            return new $transportation();
        }

        throw new TransportationNotExistException();
    }
}