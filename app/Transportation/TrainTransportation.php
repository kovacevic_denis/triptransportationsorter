<?php
namespace App\Transportation;

/**
 * Class TrainTransportation
 * @package App\Transportation
 */
class TrainTransportation extends AbstractTransportation
{
    protected $type = 'train';
}