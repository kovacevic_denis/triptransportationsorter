<?php
namespace App\Transportation;

/**
 * Class BusTransportation
 * @package App\Transportation
 */
class BusTransportation extends AbstractTransportation
{
    protected $type = 'bus';
}