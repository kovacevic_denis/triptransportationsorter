<?php
namespace App\Transportation;

use App\Contracts\Baggageable;
use App\Contracts\Gateable;

/**
 * Class PlaneTransportation
 * @package App\Transportation
 */
class PlaneTransportation extends AbstractTransportation implements Baggageable, Gateable
{
    protected $type = 'flight';

    /** @var string */
    protected $gate;

    protected $baggageNumber;

    public function getBaggageNumber()
    {
        return $this->baggageNumber;
    }

    public function getGate()
    {
        return $this->gate;
    }
}