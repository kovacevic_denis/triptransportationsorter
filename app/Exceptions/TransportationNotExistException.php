<?php

namespace App\Exceptions;

/**
 * Class TransportationNotExistException
 * @package App\Exceptions
 */
class TransportationNotExistException extends AbstractException
{

}