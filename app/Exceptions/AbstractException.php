<?php

namespace App\Exceptions;

/**
 * Class AbstractException
 * @package App\Exceptions
 */
abstract class AbstractException extends \Exception
{

}