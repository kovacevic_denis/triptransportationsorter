<?php

namespace App\Sorters;

use App\Transportation\AbstractTransportation;

/**
 * Class TransportationsSorter
 * @package App\Sorters
 */
class TransportationsSorter
{
    /**
     * @param $transportations
     * @param AbstractTransportation $startingPosition
     * @return AbstractTransportation[]
     */
    public function sort(array $transportations, AbstractTransportation $startingPosition)
    {
        /** @var AbstractTransportation[] $sorted */
        $sorted = [$startingPosition];
        do {
            foreach ($transportations as $transportation) {
                if (end($sorted)->getTo() == $transportation->getFrom()) {
                    $sorted[] = $transportation;
                    break;
                }
            }
        } while (count($sorted) < count($transportations));

        return $sorted;
    }
}