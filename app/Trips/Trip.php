<?php

namespace App\Trips;

use App\SourceDestinationWeight;
use App\Transportation\AbstractTransportation;
use App\Transportation\TransportationFactory;

class Trip
{
    /** @var AbstractTransportation[] */
    protected $transportations;

    /** @var AbstractTransportation */
    protected $startingPoint;

    public function __construct(array $transportationsConfig)
    {
        $this->buildTransportations($transportationsConfig);
    }

    /**
     * @return AbstractTransportation[]
     */
    public function getTransportations(): array
    {
        return $this->transportations;
    }

    /**
     * @return AbstractTransportation
     */
    public function getStartingPoint(): AbstractTransportation
    {
        return $this->startingPoint;
    }

    /**
     * @param array $transportationsConfig
     * @throws \App\Exceptions\TransportationNotExistException
     */
    protected function buildTransportations(array $transportationsConfig)
    {
        $transportations = [];
        $from = [];
        $to = [];

        foreach ($transportationsConfig as $transportationConfig) {
            $transportation = TransportationFactory::make($transportationConfig['transportation']);
            $transportation->setAttributes($transportationConfig);

            $from[$transportation->getFrom()] = $transportation;
            $to[] = $transportation->getTo();

            $transportations[] = $transportation;
        }
        $dif = array_diff(array_keys($from), $to);
        $dif = array_shift($dif);

        $this->transportations = $transportations;
        $this->startingPoint = $from[$dif];
    }
}