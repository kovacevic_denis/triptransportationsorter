<?php
require_once __DIR__ . '/vendor/autoload.php';

$tripsCong = loadTrips();

foreach ($tripsCong as $tripConfig) {
    $transportations = $tripConfig['transportations'];

    /** randomize transportations order */
    shuffle($transportations);

    $trip = new \App\Trips\Trip($transportations);

    $transportationsSorter = new \App\Sorters\TransportationsSorter();

    $sortedTransportations = $transportationsSorter->sort(
        $trip->getTransportations(),
        $trip->getStartingPoint()
    );

    $tripPrinter = new \App\Printers\TripPrinter($sortedTransportations);
    echo $tripPrinter->print();
    echo "\n";
}

